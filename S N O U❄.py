"""
❄ .S.N.O.U. ❄

Version: beta 0.3.3
"""

import json
import os.path
import time

import neocities

# todo: debugging mode

ignore = ['git', 'test', 'unused', '.idea', "bootstrap"]

site_dir = ''
nc = neocities.NeoCities(api_key='')
file_index = os.getcwd() + "/fileindex.json"  # may add an option to have it scan subfolders too, this'll do for now tho
version = '0.3.3'


def file_is_empty(path):
    try:
        return os.path.getsize(path) == 0
    except FileNotFoundError:
        return True


# ----------

def update_file(read_item):  # maybe a+ & change only what's needed.... too much of a bother but to maybe do
    with open(file_index, 'w') as fileindex:
        json_out = {
            'dir': site_dir,
            'neo_key': nc.api_key,
            'count': len(read_item),
            'ignore': ignore,
            'file_list': read_item
        }
        fileindex.write(json.dumps(json_out, indent=2))


def scan_dir():
    read_item = []
    for subdir, dirs, files in os.walk(site_dir):
        for file_name in files:
            file_path = subdir.replace(site_dir, '') + '/' + file_name
            # where files come from basically
            path_in_str = str(file_path)
            # ignore list check, ain't it great
            if not [ele for ele in ignore if (ele in path_in_str)]:
                # adding date along with file location
                file_list = {  # {'file': {}  } maybe if i think of a use for it
                    'file_name': path_in_str,
                    'file_time': time.ctime(os.path.getatime(subdir + '/' + file_name))
                }
                read_item.append(file_list)
    read_item = sorted(read_item, key=lambda k: k['file_name'])
    return read_item


def neoindex():
    global site_dir
    global nc
    global ignore

    if file_is_empty(file_index):
        print('-! FIRST TIME RUN -!\n')
        site_dir = input("-- INSERT SITE DIRECTORY -- \n")
        a = input("-- INSERT NEOCITIES API KEY -- \n")
        nc = neocities.NeoCities(api_key=a)
        print(' -! IGNORE LIST NOT FULLY IMPLEMENTED YET- \n'
              ' -- TO ADD THINGS TO IT FOR NOW YOU HAVE TO MANUALLY EDIT THE JSON -- ')
        # print('-- WOULD YOU LIKE TO ADD ANY FILES TO THE IGNORE LIST? --') todo: make the ignore list inserter thing
        #  on enter w no chars break out of an for loop, inside the for loop add a like 10 sec timer to auto break
        # ! no auto-break on first run
        read_item = scan_dir()
    else:

        found_item = []
        # load json & site dir
        with open(file_index) as jsonobj:
            json_item = json.load(jsonobj)
        # load global vars
        site_dir = json_item['dir']
        ignore = json_item['ignore']
        nc = neocities.NeoCities(api_key=json_item['neo_key'])

        read_item = scan_dir()
        # date checks
        for filer in read_item:
            for filej in json_item['file_list']:
                if filer['file_name'] == filej['file_name'] and filer['file_time'] != filej['file_time']:
                    found_item.append(filer)

        # missing name checks
        # this is a bad way to do this but it'll do for now
        for filer in read_item:
            found = 0
            for filej in json_item['file_list']:
                if filer['file_name'] == filej['file_name']:
                    found += 1
            # print(found)
            if found == 0:
                found_item.append(filer)

        found_item = sorted(found_item, key=lambda k: k['file_name'])

        # todo: check if you can upload beforehand
        for file in found_item:
            neoupp(file['file_name'])

    update_file(read_item)


def neoupp(file_yay):
    full_file = site_dir + file_yay
    print(" -- " + file_yay + " HAS BEEN UPLOADED -- ")
    nc.upload((full_file, file_yay))


if __name__ == '__main__':
    # start = time.time()
    neoindex()
    # end = time.time()
    # print(f"runtime of the program is {end - start}")
