# Shais_Neocities_Opensource_Uploader-S.N.O.U

S.N.O.U. ❄ is a tool written in python 3 using the neocities API made to effortlessly push changes to your site in a simple way.

It works by checking last modified dates and uploading the files that have been modified.